# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_gurizuri_session',
  :secret      => '42e72052bd89b446664adf43fcf8c706ffca5aa254301d13cc18d6c0365b4f17bbd18fb80a65d3c5a35e3dcc5825200c538b8cc155be9ae6a2394273d7763131'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
