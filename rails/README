This is a proof-of-concept project to examine how we might use the
Grizzy webserver as a Rails and/or Rack webserver.

## Status

To try out the server, first ensure you have JRuby with the following gems
installed:

- rails (2.3.3)
- activerecord-jdbcmysql-adapter (0.9.2 or newer)

Create the database using the usual Rails mechanisms:

  jruby -S rake db:create:all
  jruby -S rake db:migrate

Start the server:

  jruby script/grizzly

The application starts in production mode on port 3000.

## Code

The code currently lives in script/grizzly (server launcher) and
lib/grizzly-rack.rb (jruby-rack integration bits). Grizzly and edge
jruby-rack jars are also included.

The idea is to assume Rails threaded (threadsafe!) mode for a Rails
app, and launch the Rails application environment in the same runtime
that launches the server. The grizzly-rack machinations make this
happen, using JRuby.runtime as the destination runtime for the
jruby-rack application factory.

## TODO

- benchmark more (some early numbers in bench.numbers)
- carry out further optimizations in jruby-rack to use raw Grizzly
  request/response rather than servlet plumbing
- make a standalone project if merited:
  - standard command-line arg handling
  - gem
  - script/server support
  - generic rack support
  - documentation/tests
