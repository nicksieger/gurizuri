require 'grizzly-rack'

module Rack
  module Handler
    class Grizzly
      def self.run(app, options={})
        server = ::GrizzlyRackServer.new(app, options)
        server.run.join
      end
    end
  end
end

