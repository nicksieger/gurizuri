require 'java'
Dir[File.dirname(__FILE__) + '/*.jar'].each {|jar| require jar }

import com.sun.grizzly.http.embed.GrizzlyWebServer
import com.sun.grizzly.http.servlet.ServletAdapter
import org.jruby.rack.RackServlet
import org.jruby.rack.RackContext
import org.jruby.rack.DefaultRackApplication
import org.jruby.rack.DefaultRackApplicationFactory
import org.jruby.rack.SharedRackApplicationFactory
import org.jruby.rack.servlet.ServletRackContext
import org.jruby.rack.servlet.DefaultServletDispatcher

require 'jruby'
require 'rack/handler/servlet'
require 'jruby/rack'

class GrizzlyRackFactory < DefaultRackApplicationFactory
  def initialize(app)
    super()
    @app = app
  end

  def createApplication
    DefaultRackApplication.new
  end

  def newApplication
    createApplication
  end

  def getApplication
    app = newApplication
    app.application = createApplicationObject(JRuby.runtime)
    app
  end

  def createApplicationObject(runtime)
    Rack::Handler::Servlet.new(@app)
  end
end


class ServletRackContext
  field_accessor :context
end

class GrizzlyRackContext < ServletRackContext
  def initialize(app)
    super(nil)
    @factory = SharedRackApplicationFactory.new(GrizzlyRackFactory.new(app))
  end
  def getRackFactory
    @factory
  end
end

class GrizzlyRackServlet < RackServlet
  def init(config)
    $servlet_context.context = config.servlet_context
    $servlet_context.getRackFactory.init($servlet_context)
    super(config)
  end
end

class GrizzlyRackServer
  def initialize(app, options = {})
    @app, @options = app, options
  end

  def run
    $servlet_context = GrizzlyRackContext.new(@app)
    servlet_instance = GrizzlyRackServlet.new(DefaultServletDispatcher.new($servlet_context))

    port = @options[:Port] || 8080
    server = GrizzlyWebServer.new(port)
    servlet_adapter = ServletAdapter.new
    servlet_adapter.handle_static_resources = false
    servlet_adapter.context_path = ""
    servlet_adapter.servlet_instance = servlet_instance
    server.add_grizzly_adapter servlet_adapter
    Thread.new do
      puts "Starting server on http://localhost:#{port}"
      server.start
    end
  end
end
