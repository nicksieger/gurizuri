# -*- coding: utf-8; mode: ruby -*-
use Rack::ContentLength
run(proc {|env| [200, {"Content-Type" => "text/plain"}, ["Hello World!"]]})
